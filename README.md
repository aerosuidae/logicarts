# Logistic Carts (WIP)

Documentation and examples on the wiki: https://gitlab.com/aerosuidae/logicarts/wikis/home

*Small carts (1x1 cars) that follow paths painted on the ground.*

* Pathing is not automatic -- networks must designed carefully to avoid collision!
* Simple transport networks require only carts and paint and inserters.
* Complex routing possible via the circuit network and gates.

## Technology

* *Logistic Carts* tech (red + green science) is available after researching engines.
* *Electric Logistic Carts* tech (red + green + blue science) is available after researching electric engines.

## Carts

* Mini 1x1 tile cars
* Base speed 5km/h
* Fuel burning or Electric
* 10 trunk spaces, filterable
* Small equipment grid
* Interact with logistic network chests

## Thanks to:

* [Arch666Angel](https://mods.factorio.com/user/Arch666Angel) for graphics! (And some more yet to be released...)

## Change Log

https://gitlab.com/aerosuidae/logicarts/blob/master/changelog.md

## Development Direction

https://gitlab.com/aerosuidae/logicarts/issues